<?php

use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    public function run(){
        $companies = factory(App\Models\Company::class, 30)->make();

        foreach ($companies as $company) {
            repeat:
            try {
                $company->save();
            } catch(\Illuminate\Database\QueryException $e){
                $company = factory(App\Models\Company::class)->make();
                goto repeat;
            }
        }
    }
}
