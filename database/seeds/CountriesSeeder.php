<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    public function run(){
        $countries = factory(App\Models\Country::class, 10)->make();

        foreach ($countries as $country) {
            repeat:
            try {
                $country->save();
            } catch(\Illuminate\Database\QueryException $e){
                $country = factory(App\Models\Country::class)->make();
                goto repeat;
            }
        }
    }
}
