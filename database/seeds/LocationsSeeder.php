<?php

use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    public function run(){
        $locations = factory(App\Models\Location::class, 10)->make();

        foreach ($locations as $locations) {
            repeat:
            try{
                $locations->save();
            } catch(\Illuminate\Database\QueryException $e){
                $locations = factory(App\Models\Location::class)->make();
                goto repeat;
            }
        }
    }
}
