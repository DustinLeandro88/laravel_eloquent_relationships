<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\State;
use Faker\Generator as Faker;

$factory->define(State::class, function (Faker $faker) {
    return [
        'country_id' => $faker->numberBetween($min = 1, $max = 10),
        'name' => $faker->name,
        'initials' => $faker->lexify($string = '??'),
    ];
});
