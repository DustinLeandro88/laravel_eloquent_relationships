<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\City;
use Faker\Generator as Faker;

$factory->define(City::class, function (Faker $faker) {
    return [
        'state_id' => $faker->numberBetween($min = 1, $max = 10),
        'name' => $faker->name,
    ];
});
