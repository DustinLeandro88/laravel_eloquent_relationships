<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Location;
use Faker\Generator as Faker;

$factory->define(Location::class, function (Faker $faker) {
    return [
        'country_id' => $faker->numberBetween($min = 1, $max = 10),
        'latitude' => $faker->numberBetween($min = 100, $max = 999),
        'longitude' => $faker->numberBetween($min = 100, $max = 999),
    ];
});
