<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Location;

class OneToOneController extends Controller
{
    public function oneToOne(){
        $country = Country::where('name', 'Austria')->get()->first();
        // $country = Country::find(3);
        echo $country->name;

        $location = $country->location;
        echo "<br>Latitude: " . $location['latitude'];
        echo "<br>Longitude: " . $location['longitude'];
    }

    public function oneToOneInverse(){
        $latitude = 322;
        $longitude = 861;

        $location= Location::where('latitude', $latitude)
                            ->where('longitude', $longitude)
                            ->get()
                            ->first();
        
        $country = $location->country;

        echo $country->name;
        echo $location->id;
    }

    public function oneToOneInsert(){
        $data = [
            'name' => 'Peru',
            'latitude' => 495,
            'longitude' => 234,
        ];

        $country = Country::create($data);

        /*
        $location = new Location;
        $location->latitude = $data['latitude'];
        $location->longitude = $data['longitude'];
        $location->country_id = $country->id;
        $location->save();
        */

        $location = $country->location()->create($data);
    }
}
