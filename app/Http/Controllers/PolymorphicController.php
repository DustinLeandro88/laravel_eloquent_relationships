<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\Comment;

class PolymorphicController extends Controller
{
    public function polymorphic(){
        $country = Country::find(86);
        echo "<b>$country->name</b>";

        $comments = $country->comments()->get();

        foreach($comments as $comment){
            echo "<br>$comment->description";
        } 
    }

    public function polymorphicInsert(){
        //Comentarios city
        /*
        $city = City::where('name', 'Mylene Pfannerstill')->get()->first();
        echo "<b>$city->name</b>";

        $comment = $city->comments()->create([
            'description' => "First comment {$city->name} ".date('YmdHis'),
        ]);
        */

        //Comentarios state
        /*
        $state = State::where('name', 'Verner Mills')->get()->first();
        echo "<b>$state->name</b>";

        $comment = $state->comments()->create([
            'description' => "First comment {$state->name} ".date('YmdHis'),
        ]);
        */

        //Comentarios country
        $country = Country::find(86);
        echo "<b>$country->name</b>";

        $comment = $country->comments()->create([
            'description' => "First comment {$country->name} ".date('YmdHis'),
        ]);
    }
}
