<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;

class OneToManyController extends Controller
{
    public function oneToMany(){
        // $country = Country::where('name', 'Kenya')->get()->first();
        $keySearch = 'h';
        $countries = Country::where('name', 'LIKE', "%{$keySearch}%")->with('states')->get();

        foreach($countries as $country){
            echo "<b>($country->name)<b/>";

            $states = $country->states()->get();
            // $states = $country->states()->where('initials', 'ny')->get();

            foreach($states as $state){
                echo "<br>($state->initials) - ($state->name)";
            }

            echo '<hr>';
        }
    }

    public function oneToManyInverse(){
        $stateName= 'Pablo Murphy';
        $state = State::where('name', $stateName)->get()->first();

        echo "<b>($state->name)</b>";

        $country = $state->country()->get()->first();
        echo "<br>Pais: ($country->name)";
    }

    public function oneToManyMany(){
        // $country = Country::where('name', 'Kenya')->get()->first();
        $keySearch = 'h';
        $countries = Country::where('name', 'LIKE', "%{$keySearch}%")->with('states')->get();

        foreach($countries as $country){
            echo "<b>($country->name)<b/>";

            $states = $country->states()->get();
            // $states = $country->states()->where('initials', 'ny')->get();

            foreach($states as $state){
                echo "<br>($state->initials) - ($state->name)";

                foreach($state->cities as $city){
                    echo " $city->name,";
                }
            }

            echo '<hr>';
        }
    }

    public function oneToManyInsert(){
        $data = [
            'name' => 'Pernambuco',
            'initials' => 'PE',
        ];
        
        $country = Country::find(86);
        
        $insertState = $country->states()->create($data);
    }

    public function oneToManyInsertByState(){
        $data = [
            'name' => 'Bahia',
            'initials' => 'BA',
            'country_id' => 86,
        ];
                
        $insertState = State::create($data);
    }

    public function hasManyThrough(){
        $country = Country::find(6);
        echo "<b>$country->name:</b><br>";

        $cities = $country->cities;

        foreach($cities as $city){
            echo "$city->name, ";
        }

        echo "<br>Total de cidades: {$cities->count()}";
    }
}
