<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Company;

class ManyToManyController extends Controller
{
    public function manyToMany(){
        $city = City::where('name', 'Mylene Pfannerstill')->get()->first();
        echo "<b>$city->name:</b><br>";
        
        $companies = $city->companies;

        foreach($companies as $company){
            echo " $company->name, ";
        }
    }

    public function manyToManyInverse(){
        $company = Company::where('name', '93l')->get()->first();

        echo "<b>$company->name:</b><br>";
        
        $cities = $company->cities;
        
        foreach($cities as $city){
            echo " $city->name, ";
        }
    }

    public function manyToManyInsert(){
        $data = [7,8,9,10,11];

        $company = Company::find(2);
        echo "<b>$company->name:</b><br>";

        // $company->cities()->attach($data); <-adiciona com duplicidade
        $company->cities()->sync($data);// <-adiciona sem duplicidade
        // $company->cities()->detach($data); <-remove item ou itens array

        $cities = $company->cities;
        
        foreach($cities as $city){
            echo " $city->name, ";
        }
    }
}