<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Location;
use App\Models\State;
use App\Models\City;
use App\Models\Comment;

class Country extends Model
{
    protected $fillable = ['name'];

    public function location(){
        // return $this->hasOne(Location::class, 'country_id', 'id'); <-(class, fk_collum, collum)
        return $this->hasOne(Location::class);
    }

    public function states(){
        return $this->hasMany(State::class);
        // return $this->hasMany(State::class, 'colunaForaPadrao', 'colunaReferente');
        //                      diferente: 'country_rel' / 'id' | padrao: 'country_id' / 'id'
    }

    public function cities(){
        return $this->hasManyThrough(City::class, State::class);
    }

    public function comments(){
        return $this->morphMany(Comment::class, 'commentable');
    }
}