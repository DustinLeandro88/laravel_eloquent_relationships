<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;

class City extends Model
{
    public function companies(){
        return $this->belongsToMany(Company::class, 'company_city'); //Certo seria ordem alfa
    }

    public function comments(){
        return $this->morphMany(Comment::class, 'commentable');
    }
}
