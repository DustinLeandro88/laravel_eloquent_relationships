<?php

// One to One
Route::get('one-to-one', 'OneToOneController@oneToOne');
Route::get('one-to-one-inverse', 'OneToOneController@oneToOneInverse');
Route::get('one-to-one-insert', 'OneToOneController@oneToOneInsert');

// On to Many
Route::get('one-to-many', 'OneToManyController@oneToMany');
Route::get('one-to-many-inverse', 'OneToManyController@oneToManyInverse');
Route::get('one-to-many-many', 'OneToManyController@oneToManyMany');
Route::get('one-to-many-insert', 'OneToManyController@oneToManyInsert');
Route::get('one-to-many-insert-by-state', 'OneToManyController@oneToManyInsertByState');

// Has Many Through
Route::get('has-many-through', 'OneToManyController@hasManyThrough');

// Many to Many
Route::get('many-to-many', 'ManyToManyController@manyToMany');
Route::get('many-to-many-inverse', 'ManyToManyController@manyToManyInverse');
Route::get('many-to-many-insert', 'ManyToManyController@manyToManyInsert');

// Polymorphic
Route::get('polymorphics', 'PolymorphicController@polymorphic');
Route::get('polymorphics-insert', 'PolymorphicController@polymorphicInsert');

Route::get('/', function () {
    return view('welcome');
});